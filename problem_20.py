"""
this is the solution for problem here

https://projecteuler.net/problem=20

n! means n × (n − 1) × ... × 3 × 2 × 1

For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.

Find the sum of the digits in the number 100!

"""


def find_sum_of_the_digits(num):
    total = 0
    while True:
        next_digit = num % 10
        num = num // 10
        total = total + next_digit
        if num == 0:
            break
    return total


def factorial(n):
    if n == 0:
        return 1

    num = n * factorial(n - 1)
    return num


print(find_sum_of_the_digits(factorial(100)))
