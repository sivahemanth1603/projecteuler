"""
this ia the solution for problem here

https://projecteuler.net/problem=6


The sum of the squares of the first ten natural numbers is,

12+22+...+102=385
The square of the sum of the first ten natural numbers is,

(1+2+...+10)2=552=3025
Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025−385=2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

"""
import math


def diff_btw_sum_and_sq(n):
    total = 0
    sum_of_sq = 0

    for i in range(n):
        total = total + i
    sq_of_sum = pow(total, 2)

    for i in range(n):
        num = pow(i, 2)
        sum_of_sq = sum_of_sq + num
    dif = sq_of_sum - sum_of_sq
    return dif


print(diff_btw_sum_and_sq(101))
