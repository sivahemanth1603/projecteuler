"""
this is the solution for problem here

https://projecteuler.net/problem=10

The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.

"""


def sum_of_the_primes_below(num):
    total = 0
    for i in range(2, num):
        for j in range(2, i):
            if i % j == 0:
                break
        else:
            total = total + i
    return total


print(sum_of_the_primes_below(200000))
