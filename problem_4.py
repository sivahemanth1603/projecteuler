"""

this ia the solution for problem here

https://projecteuler.net/problem=4

A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.

Find the largest palindrome made from the product of two 3-digit numbers

"""


def reverse_num(num):
    """
    123/10 = 12,3
    12/10  = 1,2
    1/10   = 0,1
    """
    reversed_num = 0
    is_negative_num = False
    if num < 0:
        is_negative_num = True
        num = -1 * num
    while True:
        remainder = num % 10
        reversed_num = (reversed_num * 10) + remainder
        num = int(num / 10)
        if num == 0:
            break
    if is_negative_num:
        reversed_num = -1 * reversed_num

    return reversed_num


def is_palindrome(num):
    if num == reverse_num(num):
        return True
    return False


def find_largest_palindrome(digits_len):
    max_range = pow(10, digits_len)
    min_range = pow(10, digits_len - 1)
    for i in range(max_range, min_range, -1):
        for j in range(max_range, min_range, -1):
            num_product = i * j
            if is_palindrome(num_product):
                print("largest palindrome is %d * %d = %d" % (i, j, num_product))
                return
    print("can not find any palindrome")


for i in range(1, 10):
    find_largest_palindrome(i)

# print("is 1  a palindome or not " + str(is_palindrome(1)))
# print("is -68  a palindome or not " + str(is_palindrome(-68)))
# print("is 31213  a palindome or not " + str(is_palindrome(31213)))
# print("is 22  a palindome or not " + str(is_palindrome(22)))
# print("reversed num of 1 is " + str(reverse_num(1)))
# print("reversed num of 31213 is " + str(reverse_num(31213)))
# print("reversed num of -6 is " + str(reverse_num(-6)))
# print("reversed num of 22 is " + str(reverse_num(22)))
# print("reversed num of -66 is " + str(reverse_num(-66)))
