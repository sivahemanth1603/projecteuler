"""
The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:

1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...

Let us list the factors of the first seven triangle numbers:

 1: 1
 3: 1,3
 6: 1,2,3,6
10: 1,2,5,10
15: 1,3,5,15
21: 1,3,7,21
28: 1,2,4,7,14,28
We can see that 28 is the first triangle number to have over five divisors.

What is the value of the first triangle number to have over five hundred divisors?
"""


# def find_the_next_triangle_num(num):
#     next_num = int((num * (num + 1)) / 2)
#     divisors_of_triangle_num(next_num)
#
#
# def divisors_of_triangle_num(num):
#     divisors = []
#     for i in range(1, num):
#         if num % i == 0:
#             divisors.append(i)
#     if len(divisors) == 500:
#         print(num)
#         return 0
#
#
# for i in range(1, 1000):
#     find_the_next_triangle_num(i)


def get_triangle_num(num):
    triangle_num = int((num * (num + 1)) / 2)
    return triangle_num


def find_divisors(num):
    divisors = []
    for i in range(1, num):
        if num % i == 0:
            divisors.append(i)
    return divisors


def search_for_req_triangle_num(up_divs, start_from=1):
    for i in range(start_from, 10000000000):
        next_num = get_triangle_num(i)
        divs = find_divisors(next_num)
        if len(divs) == up_divs:
            return next_num, divs


# r = search_for_req_triangle_num(500, 1000000000)
# print(r)

a = get_triangle_num(100000000)
print(len(find_divisors(a)))