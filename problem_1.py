"""
This is a solution for problem here

https://projecteuler.net/problem=1

If we list all the natural numbers below 10 that are multiples of 3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.

Find the sum of all the multiples of 3 or 5 below 1000.

"""


def find_sum_until(sum_until):
    total = 0
    for i in range(sum_until):
        if i % 3 == 0 or i % 5 == 0:
            total = total + i
    return total


print(find_sum_until(1000))
