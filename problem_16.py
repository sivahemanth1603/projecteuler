"""
this is the solution for problem here

https://projecteuler.net/problem=16

215 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.

What is the sum of the digits of the number 21000?
"""


def sum_of_the_digits(num):
    sum = 0
    while True:
        last_digt = num % 10
        num = num // 10
        sum = sum + last_digt
        if num == 0:
            break
    return sum


print(sum_of_the_digits(2**10000))