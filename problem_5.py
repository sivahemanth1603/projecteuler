"""
this ia the solution for problem here

https://projecteuler.net/problem=5



2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?

"""


def find_smallest_positive_num_v1(num):
    i = 1
    while True:
        count = 0
        for j in range(1, num + 1):
            if i % j == 0:
                count = count + 1
        if count == num:
            return i
        i = i + 1


for i in range(1, 20):
    print("the smallest number that can be divided by each of the numbers from 1 to %d without any remainder is %d"%(i, find_smallest_positive_num_v1(i)))

