"""
The prime factors of 13195 are 5, 7, 13 and 29.

What is the largest prime factor of the number 600851475143 ?
"""


def largest_prime_factor(number):
    i = 2
    while number > 1:
        if number % i == 0:
            number = number/i
        else:
            i += 1
    return i


print(largest_prime_factor(600851475143))
