"""
this is the solution for problem here

https://projecteuler.net/problem=9

A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,

a2 + b2 = c2
For example, 3^2 + 4^2 = 9 + 16 = 25 = 5^2.

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

"""


def find_pythagorean_triplet():
    for i in range(1, 500):
        print("working on i %d", i)
        for j in range(i, 500):
            c_squ = ((i ** 2) + (j ** 2))
            c = find_perfect_squ(c_squ)
            if c > 0 and i + j + c == 1000:
                return i, j, c


def find_perfect_squ(num):
    for i in range(1, (num // 2)):
        if num == (i * i):
            return i
    return 0

print(find_pythagorean_triplet())
