"""

this ia the solution for problem here


https://projecteuler.net/problem=2

Each new term in the Fibonacci sequence is generated by adding the previous two terms. By starting with 1 and 2, the first 10 terms will be:

1, 2, 3, 5, 8, 13, 21, 34, 55, 89, ...

By considering the terms in the Fibonacci sequence whose values do not exceed four million, find the sum of the even-valued terms.

"""


def even_fibonacci_seq_sum_until(num):
    total = 0
    current_fib_num = 1
    next_fib_num = 2

    while True:
        if next_fib_num > num:
            return total
        else:
            temp = current_fib_num
            current_fib_num = next_fib_num
            next_fib_num = temp + current_fib_num
            if current_fib_num % 2 == 0:
                total = total + current_fib_num


max_value = 4000000

print(even_fibonacci_seq_sum_until(max_value))
