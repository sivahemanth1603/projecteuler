"""
this is the solution for problem here

https://projecteuler.net/problem=7

By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.

What is the 10 001st prime number?


"""


def find_nth_prime_num(num):
    count = 0
    i = 2
    while True:
        is_prime = True
        for j in range(2, i):
            if i % j == 0:
                is_prime = False
                break
        if is_prime:
            count = count + 1
        if count == num:
            return i
        i = i + 1


print(find_nth_prime_num(10001))
