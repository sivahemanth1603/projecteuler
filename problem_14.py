"""
this is the solution for problem here

https://projecteuler.net/problem=14

The following iterative sequence is defined for the set of positive integers:

n → n/2 (n is even)
n → 3n + 1 (n is odd)

Using the rule above and starting with 13, we generate the following sequence:

13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

"""


def find_the_seq_of_the_num(num):
    seq = [num]
    while True:
        if num == 1:
            return seq
        if num % 2 == 0:
            num = num / 2
            seq.append(int(num))
        else:
            num = (3 * num) + 1
            seq.append(int(num))


max_len = 0
seq_start = 1
for i in range(1, 1000000):
    seq = find_the_seq_of_the_num(i)
    length_of_seq = len(seq)
    if max_len < length_of_seq:
        max_len = length_of_seq
        seq_start = i
print("the num with max seq is %d and the seq is %s"%(seq_start, find_the_seq_of_the_num(seq_start)))



